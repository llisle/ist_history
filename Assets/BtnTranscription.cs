﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnTranscription : MonoBehaviour {
    private Transform Tx;
    
    // Start is called before the first frame update
    void Start() {
        Tx = transform.parent.Find("Tx");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void toggleTransciption() {
        Debug.Log("toggled");
        Tx.gameObject.SetActive(!(Tx.gameObject.activeInHierarchy));
    }
}
