﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnAudioPlayback : MonoBehaviour {

    private bool playback = false;
    private AudioSource audio;
    public AudioClip audioClip;

    // Start is called before the first frame update
    void Start() {
        //audio = GetComponent<AudioSource>();

        //Resources.Load("*", typeof(AudioClip));
    }

    // this method sets the audio to a certain file
    public void SetUpAudio(string audioName) {
        //Debug.Log("Loading audio clip at " + audioLoc);
        audio = GetComponent<AudioSource>();

        Object[] acs;

        acs = Resources.LoadAll("", typeof(AudioClip));

        foreach (var ac in acs) {
            //Debug.Log(ac.name);
            if (ac.name == audioName) {
                //Debug.Log("Found audioclip");
                audioClip = (AudioClip)ac;
            }
        }
    }

    // This method plays the audio if it is not currently playing
    public void PlayAudio() {
        Debug.Log("Playing audio from " + audio.name);
        if(!audio.isPlaying) audio.PlayOneShot(audioClip);
    }
}
