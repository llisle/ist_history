﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class VrgAttachedImage : MonoBehaviour
{
    private Texture2D _imgArtifact;

    // Use this for initialization
    new void Start() {
        // don't want it visible at start
        //gameObject.SetActive(false);
    }

    /********
     * This method loads an image onto this grabbable object.
     * keeps around the image location for potential later use.
     ********/
    public void LoadImage(string imgLoc) {
        // if image doesn't exist, report error and return
        _imgArtifact = new Texture2D(2, 2);
        ImageConversion.LoadImage(_imgArtifact, File.ReadAllBytes("Assets/Resources/" + imgLoc));
        if (_imgArtifact == null) {
            Debug.Log("Error loading image at " + imgLoc);
            return;
        }
        Rect sizer = new Rect();
        sizer.height = _imgArtifact.height;
        sizer.width = _imgArtifact.width;

        GetComponent<SpriteRenderer>().sprite = Sprite.Create(_imgArtifact, sizer, new Vector2(.5f, .5f), 3000);
    }
}
