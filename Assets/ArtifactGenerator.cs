﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using TMPro;
using System.IO;
using Debug = UnityEngine.Debug;

// This class creates all the data artifacts in the given folder
public class ArtifactGenerator : MonoBehaviour {
    private string path = "Assets/Resources/Museum";
    protected DirectoryInfo articleDir;
    
    // Enum for type of organizational scheme
    public enum Organization{
        Save, BulletinBoard, BBSmall, Museum
    }
    public Organization organization;
    
    // public enum that defines what dataset to load
    public enum Dataset {
        Tutorial, Democracy, Institution, Museum
    }
    public Dataset dataset = Dataset.Tutorial;

    // link to active commands for loading saves
    //private ActiveCommands aCommands;
    
    // public serialized variables
    public string saveStr;
    public GameObject artifacts;
    public Material altMat;
    public Transform textPrefab;
    public Transform labelPrefab;
    public Transform containerPrefab;

    // Awake runs once and then deletes the class
    public void Awake() {
        articleDir = new DirectoryInfo(path);
        string docText;
        Transform textRoot = transform.Find("TextRoot");

        // MUSEUM STYLE SETUP
        if (organization == Organization.Museum) {
            List<FileInfo[]> allExhibits = new List<FileInfo[]>();
            List<FileInfo[]> allPics = new List<FileInfo[]>();
            List<FileInfo[]> allAudio = new List<FileInfo[]>();
            DirectoryInfo[] exhibits = articleDir.GetDirectories();
            // get all the files for each exhibit
            for (int i = 0; i < exhibits.Length; i++) {
                FileInfo[] exhibitFiles = exhibits[i].GetFiles("*.txt");
                allExhibits.Add(exhibitFiles);
                FileInfo[] exhibitPics = exhibits[i].GetFiles("*.jpg");
                allPics.Add(exhibitPics);
                FileInfo[] exhibitAudio = exhibits[i].GetFiles("*.mp3");
                allAudio.Add(exhibitAudio);
            }

            // create each exhibit
            for (int i = 0; i < allExhibits.Count; i++) {
                // create an empty game object to hold the new artifacts and move them as a group
                //Transform exhibitContainer = Instantiate(containerPrefab, new Vector3(2.5f, 3.25f, -2.2f + i * 1.7f), new Quaternion(0f, 0f, 0f, 0f), transform);
                //exhibitContainer.name = "Exhibit" + i;
                //exhibitContainer.gameObject.SetActive(true);
                
                for (int j = 0; j < allExhibits[i].Length; j++) {
                    
                    // get the text and name of the exhibit artifact piece
                    docText = File.ReadAllText(allExhibits[i][j].FullName);
                    string fName;
                    //if (allExhibits[i][j].Directory.FullName != articleDir.FullName) fName = allExhibits[i][j].Directory.Name + "/" + allExhibits[i][j].Name;
                    /*else*/ fName = allExhibits[i][j].Name;

                    // see if there's a image that matches the filename
                    string imgName = allExhibits[i][j].Name.Substring(0,allExhibits[i][j].Name.Length - 4) + ".jpg";
                    FileInfo tempImg = null;
                    for (int k = 0; k < allPics[i].Length; k++) {
                        if (allPics[i][k].Name.Equals(imgName)) {
                            //Debug.Log("Found it: " + imgName);
                            tempImg = allPics[i][k];
                        }
                    }

                    // transform variables
                    Vector3 docPos = textPrefab.position;
                    Quaternion docRot = textPrefab.rotation;
                    Vector3 docScale = textPrefab.lossyScale;

                    // Create Label if we're also at j = 0
                    /*if (j == 0) {
                        Transform dirLabel = Instantiate(labelPrefab, new Vector3(52.5f, 3.25f, -2.2f + i * 1.7f), new Quaternion(0, 0, 0, 0), transform);
                        dirLabel.transform.Rotate(new Vector3(0, 1, 0), 90);
                        dirLabel.localScale = new Vector3(.8f, .25f, .003f);
                        dirLabel.gameObject.SetActive(true);
                        TextMeshPro labelText = dirLabel.Find("TextTMP").GetComponent<TextMeshPro>();
                        string labelName = allExhibits[i][j].Directory.Name;
                        labelText.SetText(labelName);
                        dirLabel.name = "Label: " + labelName;
                    }*/
                    // Organize the documents in rows of 3
                    docPos.z = 14.5f + 1.25f * (j % 5) * .5f + i * 1.7f;
                    docPos.y = 1.25f - .3f * (int)(j / 5);
                    // all on the same bulliten board
                    docPos.x = 10.635f;
                    
                    Transform doc = Instantiate(textPrefab, docPos, docRot, transform);
                    doc.localScale = docScale;
                    doc.name = fName.Substring(0, fName.Length - 4);
                    Transform docTMP = doc.Find("Tx").Find("TextTMP");
                    Transform docTitle = doc.Find("TitleTMP");
                    TextMeshPro TMP = docTMP.GetComponent<TextMeshPro>();
                    TMP.SetText(docText, true);
                    doc.gameObject.SetActive(true);
                    docTitle.GetComponent<TextMeshPro>().SetText(doc.name);
                    doc.transform.Rotate(new Vector3(0, 1, 0), 90);

                    // add image if there is one
                    if (tempImg != null) {
                        //Debug.Log(tempImg.Name + "Loading");
                        doc.Find("Image").GetComponent<VrgAttachedImage>().LoadImage("Museum/" + tempImg.Directory.Name + "/" + tempImg.Name);
                    }

                    // setup mp3 playback - if the file doesn't exist this does nothing and the placeholder remains
                    doc.Find("AudioPlayback").GetComponent<BtnAudioPlayback>().SetUpAudio(allExhibits[i][j].Name.Substring(0, allExhibits[i][j].Name.Length - 4));
                    
                    // Insert document color change if necessary here
                    if (fName.Contains("/b_")) {
                        doc.transform.GetComponent<Renderer>().material = altMat;
                    }

                    // set the Tx to be invisible
                    doc.Find("Tx").gameObject.SetActive(false);
                }
                
                // TODO: set these to behind the stage and moving past them "whooshing" into place
                //exhibitContainer.transform = new Vector3()
            }
            
            // if support is needed for any other type of initial setup, go find an older artifact generator
        }
    }
}
